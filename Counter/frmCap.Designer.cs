﻿namespace Counter
{
    partial class frmCap
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCap));
            this.vplayerCapVideo = new AForge.Controls.VideoSourcePlayer();
            this.groupBox1 = new CCWin.SkinControl.SkinGroupBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tlabelStatu = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.toolsVideoSource = new CCWin.SkinControl.SkinToolStrip();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.tcomVideoSources = new System.Windows.Forms.ToolStripComboBox();
            this.tbtnConnect = new System.Windows.Forms.ToolStripButton();
            this.tbtnDisconnect = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolsCapSetting = new CCWin.SkinControl.SkinToolStrip();
            this.toolStripLabel3 = new System.Windows.Forms.ToolStripLabel();
            this.tbtnStartCap = new System.Windows.Forms.ToolStripButton();
            this.tbtnStopCap = new System.Windows.Forms.ToolStripButton();
            this.timerRecInfo = new System.Windows.Forms.Timer(this.components);
            this.groupBox1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.toolStripContainer1.BottomToolStripPanel.SuspendLayout();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.toolsVideoSource.SuspendLayout();
            this.toolsCapSetting.SuspendLayout();
            this.SuspendLayout();
            // 
            // vplayerCapVideo
            // 
            this.vplayerCapVideo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vplayerCapVideo.Location = new System.Drawing.Point(3, 17);
            this.vplayerCapVideo.Name = "vplayerCapVideo";
            this.vplayerCapVideo.Size = new System.Drawing.Size(695, 254);
            this.vplayerCapVideo.TabIndex = 0;
            this.vplayerCapVideo.Text = "videoSourcePlayer1";
            this.vplayerCapVideo.VideoSource = null;
            this.vplayerCapVideo.NewFrame += new AForge.Controls.VideoSourcePlayer.NewFrameHandler(this.vplayerCapVideo_NewFrame);
            this.vplayerCapVideo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frmCap_KeyPress);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.BorderColor = System.Drawing.SystemColors.ActiveBorder;
            this.groupBox1.Controls.Add(this.vplayerCapVideo);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.RectBackColor = System.Drawing.Color.White;
            this.groupBox1.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.groupBox1.Size = new System.Drawing.Size(701, 274);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "实时画面";
            this.groupBox1.TitleBorderColor = System.Drawing.SystemColors.ActiveBorder;
            this.groupBox1.TitleRectBackColor = System.Drawing.Color.White;
            this.groupBox1.TitleRoundStyle = CCWin.SkinClass.RoundStyle.All;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tlabelStatu});
            this.statusStrip1.Location = new System.Drawing.Point(0, 0);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(701, 22);
            this.statusStrip1.TabIndex = 4;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tlabelStatu
            // 
            this.tlabelStatu.BackColor = System.Drawing.Color.Transparent;
            this.tlabelStatu.Name = "tlabelStatu";
            this.tlabelStatu.Size = new System.Drawing.Size(686, 17);
            this.tlabelStatu.Spring = true;
            this.tlabelStatu.Text = "状态";
            this.tlabelStatu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.BottomToolStripPanel
            // 
            this.toolStripContainer1.BottomToolStripPanel.Controls.Add(this.statusStrip1);
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.AutoScroll = true;
            this.toolStripContainer1.ContentPanel.Controls.Add(this.groupBox1);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(701, 274);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.Location = new System.Drawing.Point(4, 28);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(701, 346);
            this.toolStripContainer1.TabIndex = 1;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.toolsCapSetting);
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.toolsVideoSource);
            // 
            // toolsVideoSource
            // 
            this.toolsVideoSource.Arrow = System.Drawing.Color.Black;
            this.toolsVideoSource.Back = System.Drawing.Color.Transparent;
            this.toolsVideoSource.BackRadius = 4;
            this.toolsVideoSource.BackRectangle = new System.Drawing.Rectangle(10, 10, 10, 10);
            this.toolsVideoSource.Base = System.Drawing.Color.Transparent;
            this.toolsVideoSource.BaseFore = System.Drawing.Color.Black;
            this.toolsVideoSource.BaseForeAnamorphosis = false;
            this.toolsVideoSource.BaseForeAnamorphosisBorder = 4;
            this.toolsVideoSource.BaseForeAnamorphosisColor = System.Drawing.Color.White;
            this.toolsVideoSource.BaseForeOffset = new System.Drawing.Point(0, 0);
            this.toolsVideoSource.BaseHoverFore = System.Drawing.Color.White;
            this.toolsVideoSource.BaseItemAnamorphosis = true;
            this.toolsVideoSource.BaseItemBorder = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.toolsVideoSource.BaseItemBorderShow = true;
            this.toolsVideoSource.BaseItemDown = ((System.Drawing.Image)(resources.GetObject("toolsVideoSource.BaseItemDown")));
            this.toolsVideoSource.BaseItemHover = System.Drawing.Color.Black;
            this.toolsVideoSource.BaseItemMouse = ((System.Drawing.Image)(resources.GetObject("toolsVideoSource.BaseItemMouse")));
            this.toolsVideoSource.BaseItemNorml = null;
            this.toolsVideoSource.BaseItemPressed = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.toolsVideoSource.BaseItemRadius = 4;
            this.toolsVideoSource.BaseItemRadiusStyle = CCWin.SkinClass.RoundStyle.All;
            this.toolsVideoSource.BaseItemSplitter = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.toolsVideoSource.BindTabControl = null;
            this.toolsVideoSource.Dock = System.Windows.Forms.DockStyle.None;
            this.toolsVideoSource.DropDownImageSeparator = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(197)))), ((int)(((byte)(197)))));
            this.toolsVideoSource.Fore = System.Drawing.Color.Black;
            this.toolsVideoSource.GripMargin = new System.Windows.Forms.Padding(2, 2, 4, 2);
            this.toolsVideoSource.HoverFore = System.Drawing.Color.White;
            this.toolsVideoSource.ItemAnamorphosis = true;
            this.toolsVideoSource.ItemBorder = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.toolsVideoSource.ItemBorderShow = true;
            this.toolsVideoSource.ItemHover = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.toolsVideoSource.ItemPressed = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.toolsVideoSource.ItemRadius = 4;
            this.toolsVideoSource.ItemRadiusStyle = CCWin.SkinClass.RoundStyle.All;
            this.toolsVideoSource.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.tcomVideoSources,
            this.tbtnConnect,
            this.tbtnDisconnect,
            this.toolStripButton1});
            this.toolsVideoSource.Location = new System.Drawing.Point(3, 25);
            this.toolsVideoSource.Name = "toolsVideoSource";
            this.toolsVideoSource.RadiusStyle = CCWin.SkinClass.RoundStyle.All;
            this.toolsVideoSource.Size = new System.Drawing.Size(375, 25);
            this.toolsVideoSource.SkinAllColor = true;
            this.toolsVideoSource.TabIndex = 0;
            this.toolsVideoSource.TitleAnamorphosis = true;
            this.toolsVideoSource.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(228)))), ((int)(((byte)(236)))));
            this.toolsVideoSource.TitleRadius = 4;
            this.toolsVideoSource.TitleRadiusStyle = CCWin.SkinClass.RoundStyle.All;
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(56, 22);
            this.toolStripLabel1.Text = "视屏源：";
            // 
            // tcomVideoSources
            // 
            this.tcomVideoSources.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tcomVideoSources.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.tcomVideoSources.Name = "tcomVideoSources";
            this.tcomVideoSources.Size = new System.Drawing.Size(121, 25);
            this.tcomVideoSources.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frmCap_KeyPress);
            // 
            // tbtnConnect
            // 
            this.tbtnConnect.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnConnect.Image = ((System.Drawing.Image)(resources.GetObject("tbtnConnect.Image")));
            this.tbtnConnect.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnConnect.Name = "tbtnConnect";
            this.tbtnConnect.Size = new System.Drawing.Size(74, 22);
            this.tbtnConnect.Text = "录像(Enter)";
            this.tbtnConnect.Click += new System.EventHandler(this.tbtnConnect_Click);
            // 
            // tbtnDisconnect
            // 
            this.tbtnDisconnect.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnDisconnect.Enabled = false;
            this.tbtnDisconnect.Image = ((System.Drawing.Image)(resources.GetObject("tbtnDisconnect.Image")));
            this.tbtnDisconnect.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnDisconnect.Name = "tbtnDisconnect";
            this.tbtnDisconnect.Size = new System.Drawing.Size(36, 22);
            this.tbtnDisconnect.Text = "断开";
            this.tbtnDisconnect.Visible = false;
            this.tbtnDisconnect.Click += new System.EventHandler(this.tbtnDisconnect_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(108, 22);
            this.toolStripButton1.Text = "设置为备用录像源";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolsCapSetting
            // 
            this.toolsCapSetting.Arrow = System.Drawing.Color.Black;
            this.toolsCapSetting.Back = System.Drawing.Color.Transparent;
            this.toolsCapSetting.BackRadius = 4;
            this.toolsCapSetting.BackRectangle = new System.Drawing.Rectangle(10, 10, 10, 10);
            this.toolsCapSetting.Base = System.Drawing.Color.Transparent;
            this.toolsCapSetting.BaseFore = System.Drawing.Color.Black;
            this.toolsCapSetting.BaseForeAnamorphosis = false;
            this.toolsCapSetting.BaseForeAnamorphosisBorder = 4;
            this.toolsCapSetting.BaseForeAnamorphosisColor = System.Drawing.Color.White;
            this.toolsCapSetting.BaseForeOffset = new System.Drawing.Point(0, 0);
            this.toolsCapSetting.BaseHoverFore = System.Drawing.Color.White;
            this.toolsCapSetting.BaseItemAnamorphosis = true;
            this.toolsCapSetting.BaseItemBorder = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.toolsCapSetting.BaseItemBorderShow = true;
            this.toolsCapSetting.BaseItemDown = ((System.Drawing.Image)(resources.GetObject("toolsCapSetting.BaseItemDown")));
            this.toolsCapSetting.BaseItemHover = System.Drawing.Color.Black;
            this.toolsCapSetting.BaseItemMouse = ((System.Drawing.Image)(resources.GetObject("toolsCapSetting.BaseItemMouse")));
            this.toolsCapSetting.BaseItemNorml = null;
            this.toolsCapSetting.BaseItemPressed = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.toolsCapSetting.BaseItemRadius = 4;
            this.toolsCapSetting.BaseItemRadiusStyle = CCWin.SkinClass.RoundStyle.All;
            this.toolsCapSetting.BaseItemSplitter = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.toolsCapSetting.BindTabControl = null;
            this.toolsCapSetting.Dock = System.Windows.Forms.DockStyle.None;
            this.toolsCapSetting.DropDownImageSeparator = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(197)))), ((int)(((byte)(197)))));
            this.toolsCapSetting.Enabled = false;
            this.toolsCapSetting.Fore = System.Drawing.Color.Black;
            this.toolsCapSetting.GripMargin = new System.Windows.Forms.Padding(2, 2, 4, 2);
            this.toolsCapSetting.HoverFore = System.Drawing.Color.White;
            this.toolsCapSetting.ItemAnamorphosis = true;
            this.toolsCapSetting.ItemBorder = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.toolsCapSetting.ItemBorderShow = true;
            this.toolsCapSetting.ItemHover = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.toolsCapSetting.ItemPressed = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.toolsCapSetting.ItemRadius = 4;
            this.toolsCapSetting.ItemRadiusStyle = CCWin.SkinClass.RoundStyle.All;
            this.toolsCapSetting.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel3,
            this.tbtnStartCap,
            this.tbtnStopCap});
            this.toolsCapSetting.Location = new System.Drawing.Point(3, 0);
            this.toolsCapSetting.Name = "toolsCapSetting";
            this.toolsCapSetting.RadiusStyle = CCWin.SkinClass.RoundStyle.All;
            this.toolsCapSetting.Size = new System.Drawing.Size(272, 25);
            this.toolsCapSetting.SkinAllColor = true;
            this.toolsCapSetting.TabIndex = 1;
            this.toolsCapSetting.Text = "0";
            this.toolsCapSetting.TitleAnamorphosis = true;
            this.toolsCapSetting.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(228)))), ((int)(((byte)(236)))));
            this.toolsCapSetting.TitleRadius = 4;
            this.toolsCapSetting.TitleRadiusStyle = CCWin.SkinClass.RoundStyle.All;
            // 
            // toolStripLabel3
            // 
            this.toolStripLabel3.Name = "toolStripLabel3";
            this.toolStripLabel3.Size = new System.Drawing.Size(68, 22);
            this.toolStripLabel3.Text = "录像控制：";
            // 
            // tbtnStartCap
            // 
            this.tbtnStartCap.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnStartCap.Image = ((System.Drawing.Image)(resources.GetObject("tbtnStartCap.Image")));
            this.tbtnStartCap.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnStartCap.Name = "tbtnStartCap";
            this.tbtnStartCap.Size = new System.Drawing.Size(103, 22);
            this.tbtnStartCap.Text = "开始计时(Space)";
            this.tbtnStartCap.Click += new System.EventHandler(this.tbtnStartCap_Click);
            // 
            // tbtnStopCap
            // 
            this.tbtnStopCap.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnStopCap.Enabled = false;
            this.tbtnStopCap.Image = ((System.Drawing.Image)(resources.GetObject("tbtnStopCap.Image")));
            this.tbtnStopCap.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnStopCap.Name = "tbtnStopCap";
            this.tbtnStopCap.Size = new System.Drawing.Size(87, 22);
            this.tbtnStopCap.Text = "停止录制(Esc)";
            this.tbtnStopCap.Click += new System.EventHandler(this.tbtnStopCap_Click);
            // 
            // timerRecInfo
            // 
            this.timerRecInfo.Interval = 1000;
            this.timerRecInfo.Tick += new System.EventHandler(this.timerRecInfo_Tick);
            // 
            // frmCap
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(709, 378);
            this.Controls.Add(this.toolStripContainer1);
            this.InnerBorderColor = System.Drawing.Color.Black;
            this.Name = "frmCap";
            this.ShowSystemMenu = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "录像窗口";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.formCap_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmCap_FormClosed);
            this.Load += new System.EventHandler(this.formCap_Load);
            this.Shown += new System.EventHandler(this.frmCap_Shown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frmCap_KeyPress);
            this.groupBox1.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.toolStripContainer1.BottomToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.BottomToolStripPanel.PerformLayout();
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.toolsVideoSource.ResumeLayout(false);
            this.toolsVideoSource.PerformLayout();
            this.toolsCapSetting.ResumeLayout(false);
            this.toolsCapSetting.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private AForge.Controls.VideoSourcePlayer vplayerCapVideo;
        private CCWin.SkinControl.SkinGroupBox groupBox1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tlabelStatu;
        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private CCWin.SkinControl.SkinToolStrip toolsVideoSource;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripComboBox tcomVideoSources;
        private System.Windows.Forms.ToolStripButton tbtnConnect;
        private CCWin.SkinControl.SkinToolStrip toolsCapSetting;
        private System.Windows.Forms.ToolStripLabel toolStripLabel3;
        private System.Windows.Forms.ToolStripButton tbtnStartCap;
        private System.Windows.Forms.ToolStripButton tbtnStopCap;
        private System.Windows.Forms.ToolStripButton tbtnDisconnect;
        private System.Windows.Forms.Timer timerRecInfo;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
    }
}

