﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Counter
{
    public partial class frmCompInfoSel : UserControl
    {
        private string[] ReadSetting(string name)
        {
            List<string> res = new List<string>();
            if(!Directory.Exists(Application.StartupPath + @"\Settings\"))
            {
                Directory.CreateDirectory(Application.StartupPath + @"\Settings\");
            }
            if(!File.Exists(Application.StartupPath + @"\Settings\" + name + ".txt"))
            {
                File.Create(Application.StartupPath + @"\Settings\" + name + ".txt").Close();
                return res.ToArray();
            }
            FileStream stream = File.OpenRead(Application.StartupPath + @"\Settings\" + name + ".txt");
            using (StreamReader reader = new StreamReader(stream, Encoding.Default))
            {
                while (!reader.EndOfStream)
                    res.Add(reader.ReadLine());
            }
            return res.ToArray();
        }
        public frmCompInfoSel()
        {
            InitializeComponent();
            //读取默认设置
            this.comGroupType.Items.AddRange(ReadSetting("组别"));
            this.comProject.Items.AddRange(ReadSetting("项目"));
            this.comComID.Items.AddRange(ReadSetting("赛次"));
            this.comGroupID.Items.AddRange(ReadSetting("组次"));
        }
        private void btnOK_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
