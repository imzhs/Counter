﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Counter
{
    static class Program
    {
        public static frmMain mainForm ;
        public static IniFiles setting = new IniFiles(Application.StartupPath + @"\Settings.ini");
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            if (DateTime.Now < new DateTime(2015, 9, 16) || DateTime.Now > new DateTime(2015, 11, 30))
            {
                MessageBox.Show("本版本评估已失效！请使用下个版本程序！", "严重错误", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            mainForm = new frmMain();
            
            Application.Run(mainForm);
        }
    }
}
