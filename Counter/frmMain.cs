﻿using CCWin;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Counter
{
    public partial class frmMain : CCSkinMain
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void skinButton1_Click(object sender, EventArgs e)
        {
            new frmCap(false).Show();
            this.Hide();
        }

        private void skinButton2_Click(object sender, EventArgs e)
        {
            new frmCount().Show();
            this.Hide();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
        }

        private void frmMain_Shown(object sender, EventArgs e)
        {
            MessageBox.Show("请选择要执行的操作\n本版本仅用于评估！评估期限为2015/11/30\n超过评估期限将无法正常使用本程序！","评估版本",MessageBoxButtons.OK,MessageBoxIcon.Asterisk);
        }

        private void frmMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
            Environment.Exit(0);
        }
    }
}
