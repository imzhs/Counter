﻿namespace Counter
{
    partial class frmCount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCount));
            this.splitContainer1 = new CCWin.SkinControl.SkinSplitContainer();
            this.skinSplitContainer2 = new CCWin.SkinControl.SkinSplitContainer();
            this.groupBox2 = new CCWin.SkinControl.SkinGroupBox();
            this.playerRec = new AxWMPLib.AxWindowsMediaPlayer();
            this.groupBox1 = new CCWin.SkinControl.SkinGroupBox();
            this.trackLoc = new CCWin.SkinControl.SkinTrackBar();
            this.gpTrim = new CCWin.SkinControl.SkinGroupBox();
            this.panel5 = new CCWin.SkinControl.SkinPanel();
            this.trackMillionSec = new CCWin.SkinControl.SkinTrackBar();
            this.panel6 = new CCWin.SkinControl.SkinPanel();
            this.label3 = new CCWin.SkinControl.SkinLabel();
            this.numMillionSec = new CCWin.SkinControl.SkinNumericUpDown();
            this.panel3 = new CCWin.SkinControl.SkinPanel();
            this.trackSec = new CCWin.SkinControl.SkinTrackBar();
            this.panel4 = new CCWin.SkinControl.SkinPanel();
            this.label2 = new CCWin.SkinControl.SkinLabel();
            this.numSec = new CCWin.SkinControl.SkinNumericUpDown();
            this.panel1 = new CCWin.SkinControl.SkinPanel();
            this.trackMin = new CCWin.SkinControl.SkinTrackBar();
            this.panel2 = new CCWin.SkinControl.SkinPanel();
            this.label1 = new CCWin.SkinControl.SkinLabel();
            this.numMin = new CCWin.SkinControl.SkinNumericUpDown();
            this.skinGroupBox2 = new CCWin.SkinControl.SkinGroupBox();
            this.skinPanel3 = new CCWin.SkinControl.SkinPanel();
            this.skinPanel7 = new CCWin.SkinControl.SkinPanel();
            this.txtTag = new CCWin.SkinControl.SkinTextBox();
            this.skinLabel6 = new CCWin.SkinControl.SkinLabel();
            this.skinPanel5 = new CCWin.SkinControl.SkinPanel();
            this.txtArriveTime = new CCWin.SkinControl.SkinTextBox();
            this.btnSetArriveTime = new CCWin.SkinControl.SkinButton();
            this.skinLabel4 = new CCWin.SkinControl.SkinLabel();
            this.skinPanel4 = new CCWin.SkinControl.SkinPanel();
            this.txtStartTime = new CCWin.SkinControl.SkinTextBox();
            this.btnSetStartTime = new CCWin.SkinControl.SkinButton();
            this.skinLabel3 = new CCWin.SkinControl.SkinLabel();
            this.skinPanel2 = new CCWin.SkinControl.SkinPanel();
            this.txtName = new CCWin.SkinControl.SkinTextBox();
            this.skinLabel2 = new CCWin.SkinControl.SkinLabel();
            this.skinPanel1 = new CCWin.SkinControl.SkinPanel();
            this.txtID = new CCWin.SkinControl.SkinTextBox();
            this.skinLabel1 = new CCWin.SkinControl.SkinLabel();
            this.skinPanel6 = new CCWin.SkinControl.SkinPanel();
            this.txtLineID = new CCWin.SkinControl.SkinTextBox();
            this.skinLabel5 = new CCWin.SkinControl.SkinLabel();
            this.skinGroupBox1 = new CCWin.SkinControl.SkinGroupBox();
            this.skinToolStrip1 = new CCWin.SkinControl.SkinToolStrip();
            this.toolStripLabel3 = new System.Windows.Forms.ToolStripLabel();
            this.tbtnAddMember = new System.Windows.Forms.ToolStripButton();
            this.tbtnSelAll = new System.Windows.Forms.ToolStripButton();
            this.tbtnUnselAll = new System.Windows.Forms.ToolStripButton();
            this.tbtnReverseSel = new System.Windows.Forms.ToolStripButton();
            this.tbtnRemoveSel = new System.Windows.Forms.ToolStripButton();
            this.tbtnPrint = new System.Windows.Forms.ToolStripButton();
            this.tbtnSave = new System.Windows.Forms.ToolStripButton();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tlabelInfo = new System.Windows.Forms.ToolStripStatusLabel();
            this.tlabelStatu = new System.Windows.Forms.ToolStripStatusLabel();
            this.tlabelTime = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolsRecSettting = new CCWin.SkinControl.SkinToolStrip();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.tcomRecFile = new System.Windows.Forms.ToolStripComboBox();
            this.tbtnSelRec = new System.Windows.Forms.ToolStripButton();
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.toolsPlaySetting = new CCWin.SkinControl.SkinToolStrip();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.tbtnPlay = new System.Windows.Forms.ToolStripButton();
            this.tbtnStop = new System.Windows.Forms.ToolStripButton();
            this.tbtnFastForward = new System.Windows.Forms.ToolStripButton();
            this.tbtnFastBackward = new System.Windows.Forms.ToolStripButton();
            this.tbtnTrim = new System.Windows.Forms.ToolStripButton();
            this.timerPlayTime = new System.Windows.Forms.Timer(this.components);
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.timerTimeSet = new System.Windows.Forms.Timer(this.components);
            this.info = new Counter.frmCompInfoSel();
            this.lstviewMembers = new Counter.PrintListView();
            this.colLineID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colStartTime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colArriveTime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colCost = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colScore = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colTag = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.skinSplitContainer2)).BeginInit();
            this.skinSplitContainer2.Panel1.SuspendLayout();
            this.skinSplitContainer2.Panel2.SuspendLayout();
            this.skinSplitContainer2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.playerRec)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackLoc)).BeginInit();
            this.gpTrim.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackMillionSec)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numMillionSec)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackSec)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSec)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackMin)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numMin)).BeginInit();
            this.skinGroupBox2.SuspendLayout();
            this.skinPanel3.SuspendLayout();
            this.skinPanel7.SuspendLayout();
            this.skinPanel5.SuspendLayout();
            this.txtArriveTime.SuspendLayout();
            this.skinPanel4.SuspendLayout();
            this.txtStartTime.SuspendLayout();
            this.skinPanel2.SuspendLayout();
            this.skinPanel1.SuspendLayout();
            this.skinPanel6.SuspendLayout();
            this.skinGroupBox1.SuspendLayout();
            this.skinToolStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.toolsRecSettting.SuspendLayout();
            this.toolStripContainer1.BottomToolStripPanel.SuspendLayout();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.toolsPlaySetting.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.ArroColor = System.Drawing.Color.Black;
            this.splitContainer1.ArroHoverColor = System.Drawing.Color.White;
            this.splitContainer1.CollapsePanel = CCWin.SkinControl.CollapsePanel.Panel2;
            this.splitContainer1.Cursor = System.Windows.Forms.Cursors.Default;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Enabled = false;
            this.splitContainer1.LineBack = System.Drawing.Color.DimGray;
            this.splitContainer1.LineBack2 = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.skinSplitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.skinGroupBox1);
            this.splitContainer1.Panel2MinSize = 0;
            this.splitContainer1.Size = new System.Drawing.Size(896, 623);
            this.splitContainer1.SplitterDistance = 422;
            this.splitContainer1.TabIndex = 0;
            // 
            // skinSplitContainer2
            // 
            this.skinSplitContainer2.ArroColor = System.Drawing.Color.Black;
            this.skinSplitContainer2.Cursor = System.Windows.Forms.Cursors.Default;
            this.skinSplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skinSplitContainer2.LineBack = System.Drawing.Color.DimGray;
            this.skinSplitContainer2.LineBack2 = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(244)))), ((int)(((byte)(244)))));
            this.skinSplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.skinSplitContainer2.Name = "skinSplitContainer2";
            // 
            // skinSplitContainer2.Panel1
            // 
            this.skinSplitContainer2.Panel1.Controls.Add(this.groupBox2);
            // 
            // skinSplitContainer2.Panel2
            // 
            this.skinSplitContainer2.Panel2.Controls.Add(this.skinGroupBox2);
            this.skinSplitContainer2.Size = new System.Drawing.Size(896, 422);
            this.skinSplitContainer2.SplitterDistance = 661;
            this.skinSplitContainer2.TabIndex = 2;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.BorderColor = System.Drawing.SystemColors.ActiveBorder;
            this.groupBox2.Controls.Add(this.playerRec);
            this.groupBox2.Controls.Add(this.groupBox1);
            this.groupBox2.Controls.Add(this.gpTrim);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.ForeColor = System.Drawing.Color.Black;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.RectBackColor = System.Drawing.Color.Transparent;
            this.groupBox2.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.groupBox2.Size = new System.Drawing.Size(661, 422);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "录像播放";
            this.groupBox2.TitleBorderColor = System.Drawing.Color.Black;
            this.groupBox2.TitleRectBackColor = System.Drawing.Color.White;
            this.groupBox2.TitleRoundStyle = CCWin.SkinClass.RoundStyle.All;
            // 
            // playerRec
            // 
            this.playerRec.Dock = System.Windows.Forms.DockStyle.Fill;
            this.playerRec.Enabled = true;
            this.playerRec.Location = new System.Drawing.Point(3, 17);
            this.playerRec.Name = "playerRec";
            this.playerRec.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("playerRec.OcxState")));
            this.playerRec.Size = new System.Drawing.Size(655, 247);
            this.playerRec.TabIndex = 3;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.BorderColor = System.Drawing.SystemColors.ActiveBorder;
            this.groupBox1.Controls.Add(this.trackLoc);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox1.ForeColor = System.Drawing.Color.Black;
            this.groupBox1.Location = new System.Drawing.Point(3, 264);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.RectBackColor = System.Drawing.Color.Transparent;
            this.groupBox1.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.groupBox1.Size = new System.Drawing.Size(655, 54);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "位置调整";
            this.groupBox1.TitleBorderColor = System.Drawing.Color.Black;
            this.groupBox1.TitleRectBackColor = System.Drawing.Color.White;
            this.groupBox1.TitleRoundStyle = CCWin.SkinClass.RoundStyle.All;
            // 
            // trackLoc
            // 
            this.trackLoc.BackColor = System.Drawing.Color.Transparent;
            this.trackLoc.Bar = null;
            this.trackLoc.BarStyle = CCWin.SkinControl.HSLTrackBarStyle.Saturation;
            this.trackLoc.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.trackLoc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trackLoc.Location = new System.Drawing.Point(3, 17);
            this.trackLoc.Maximum = 1;
            this.trackLoc.Name = "trackLoc";
            this.trackLoc.Size = new System.Drawing.Size(649, 45);
            this.trackLoc.TabIndex = 0;
            this.trackLoc.Track = null;
            this.trackLoc.Value = 1;
            this.trackLoc.ValueChanged += new System.EventHandler(this.trackLoc_ValueChanged);
            this.trackLoc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.trackLoc_KeyDown);
            this.trackLoc.KeyUp += new System.Windows.Forms.KeyEventHandler(this.trackLoc_KeyUp);
            this.trackLoc.MouseDown += new System.Windows.Forms.MouseEventHandler(this.trackLoc_MouseDown);
            this.trackLoc.MouseUp += new System.Windows.Forms.MouseEventHandler(this.trackLoc_MouseUp);
            // 
            // gpTrim
            // 
            this.gpTrim.BackColor = System.Drawing.Color.Transparent;
            this.gpTrim.BorderColor = System.Drawing.SystemColors.ActiveBorder;
            this.gpTrim.Controls.Add(this.panel5);
            this.gpTrim.Controls.Add(this.panel3);
            this.gpTrim.Controls.Add(this.panel1);
            this.gpTrim.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.gpTrim.ForeColor = System.Drawing.Color.Black;
            this.gpTrim.Location = new System.Drawing.Point(3, 318);
            this.gpTrim.Name = "gpTrim";
            this.gpTrim.RectBackColor = System.Drawing.Color.Transparent;
            this.gpTrim.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.gpTrim.Size = new System.Drawing.Size(655, 101);
            this.gpTrim.TabIndex = 1;
            this.gpTrim.TabStop = false;
            this.gpTrim.Text = "位置微调";
            this.gpTrim.TitleBorderColor = System.Drawing.Color.Black;
            this.gpTrim.TitleRectBackColor = System.Drawing.Color.White;
            this.gpTrim.TitleRoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.gpTrim.Visible = false;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Transparent;
            this.panel5.Controls.Add(this.trackMillionSec);
            this.panel5.Controls.Add(this.panel6);
            this.panel5.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.DownBack = null;
            this.panel5.Location = new System.Drawing.Point(3, 67);
            this.panel5.MouseBack = null;
            this.panel5.Name = "panel5";
            this.panel5.NormlBack = null;
            this.panel5.Size = new System.Drawing.Size(649, 25);
            this.panel5.TabIndex = 2;
            // 
            // trackMillionSec
            // 
            this.trackMillionSec.BackColor = System.Drawing.Color.Transparent;
            this.trackMillionSec.Bar = null;
            this.trackMillionSec.BarStyle = CCWin.SkinControl.HSLTrackBarStyle.Saturation;
            this.trackMillionSec.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.trackMillionSec.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trackMillionSec.Location = new System.Drawing.Point(0, 0);
            this.trackMillionSec.Maximum = 1;
            this.trackMillionSec.Name = "trackMillionSec";
            this.trackMillionSec.Size = new System.Drawing.Size(557, 45);
            this.trackMillionSec.TabIndex = 2;
            this.trackMillionSec.Track = null;
            this.trackMillionSec.Value = 1;
            this.trackMillionSec.ValueChanged += new System.EventHandler(this.trackMillionSec_ValueChanged);
            this.trackMillionSec.KeyDown += new System.Windows.Forms.KeyEventHandler(this.trackLoc_KeyDown);
            this.trackMillionSec.KeyUp += new System.Windows.Forms.KeyEventHandler(this.trackLoc_KeyUp);
            this.trackMillionSec.MouseDown += new System.Windows.Forms.MouseEventHandler(this.trackMin_MouseUp);
            this.trackMillionSec.MouseUp += new System.Windows.Forms.MouseEventHandler(this.trackMin_MouseUp);
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Transparent;
            this.panel6.Controls.Add(this.label3);
            this.panel6.Controls.Add(this.numMillionSec);
            this.panel6.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.panel6.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel6.DownBack = null;
            this.panel6.Location = new System.Drawing.Point(557, 0);
            this.panel6.MouseBack = null;
            this.panel6.Name = "panel6";
            this.panel6.NormlBack = null;
            this.panel6.Size = new System.Drawing.Size(92, 25);
            this.panel6.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.BorderColor = System.Drawing.Color.White;
            this.label3.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(63, 7);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 17);
            this.label3.TabIndex = 1;
            this.label3.Text = "毫秒";
            // 
            // numMillionSec
            // 
            this.numMillionSec.ArrowColor = System.Drawing.Color.Black;
            this.numMillionSec.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.numMillionSec.BorderColor = System.Drawing.Color.Black;
            this.numMillionSec.Location = new System.Drawing.Point(3, 2);
            this.numMillionSec.Name = "numMillionSec";
            this.numMillionSec.Size = new System.Drawing.Size(56, 21);
            this.numMillionSec.TabIndex = 0;
            this.numMillionSec.ValueChanged += new System.EventHandler(this.numMillionSec_ValueChanged);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Transparent;
            this.panel3.Controls.Add(this.trackSec);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.DownBack = null;
            this.panel3.Location = new System.Drawing.Point(3, 42);
            this.panel3.MouseBack = null;
            this.panel3.Name = "panel3";
            this.panel3.NormlBack = null;
            this.panel3.Size = new System.Drawing.Size(649, 25);
            this.panel3.TabIndex = 1;
            // 
            // trackSec
            // 
            this.trackSec.BackColor = System.Drawing.Color.Transparent;
            this.trackSec.Bar = null;
            this.trackSec.BarStyle = CCWin.SkinControl.HSLTrackBarStyle.Saturation;
            this.trackSec.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.trackSec.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trackSec.Location = new System.Drawing.Point(0, 0);
            this.trackSec.Maximum = 1;
            this.trackSec.Name = "trackSec";
            this.trackSec.Size = new System.Drawing.Size(557, 45);
            this.trackSec.TabIndex = 2;
            this.trackSec.Track = null;
            this.trackSec.Value = 1;
            this.trackSec.ValueChanged += new System.EventHandler(this.trackSec_ValueChanged);
            this.trackSec.KeyDown += new System.Windows.Forms.KeyEventHandler(this.trackLoc_KeyDown);
            this.trackSec.KeyUp += new System.Windows.Forms.KeyEventHandler(this.trackLoc_KeyUp);
            this.trackSec.MouseDown += new System.Windows.Forms.MouseEventHandler(this.trackMin_MouseUp);
            this.trackSec.MouseUp += new System.Windows.Forms.MouseEventHandler(this.trackMin_MouseUp);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Transparent;
            this.panel4.Controls.Add(this.label2);
            this.panel4.Controls.Add(this.numSec);
            this.panel4.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.DownBack = null;
            this.panel4.Location = new System.Drawing.Point(557, 0);
            this.panel4.MouseBack = null;
            this.panel4.Name = "panel4";
            this.panel4.NormlBack = null;
            this.panel4.Size = new System.Drawing.Size(92, 25);
            this.panel4.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.BorderColor = System.Drawing.Color.White;
            this.label2.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(63, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(20, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "秒";
            // 
            // numSec
            // 
            this.numSec.ArrowColor = System.Drawing.Color.Black;
            this.numSec.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.numSec.BorderColor = System.Drawing.Color.Black;
            this.numSec.Location = new System.Drawing.Point(3, 2);
            this.numSec.Name = "numSec";
            this.numSec.Size = new System.Drawing.Size(56, 21);
            this.numSec.TabIndex = 0;
            this.numSec.ValueChanged += new System.EventHandler(this.numSec_ValueChanged);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.trackMin);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.DownBack = null;
            this.panel1.Location = new System.Drawing.Point(3, 17);
            this.panel1.MouseBack = null;
            this.panel1.Name = "panel1";
            this.panel1.NormlBack = null;
            this.panel1.Size = new System.Drawing.Size(649, 25);
            this.panel1.TabIndex = 0;
            // 
            // trackMin
            // 
            this.trackMin.BackColor = System.Drawing.Color.Transparent;
            this.trackMin.Bar = null;
            this.trackMin.BarStyle = CCWin.SkinControl.HSLTrackBarStyle.Saturation;
            this.trackMin.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.trackMin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trackMin.Location = new System.Drawing.Point(0, 0);
            this.trackMin.Maximum = 1;
            this.trackMin.Name = "trackMin";
            this.trackMin.Size = new System.Drawing.Size(557, 45);
            this.trackMin.TabIndex = 1;
            this.trackMin.Track = null;
            this.trackMin.Value = 1;
            this.trackMin.ValueChanged += new System.EventHandler(this.trackMin_ValueChanged);
            this.trackMin.KeyDown += new System.Windows.Forms.KeyEventHandler(this.trackLoc_KeyDown);
            this.trackMin.KeyUp += new System.Windows.Forms.KeyEventHandler(this.trackLoc_KeyUp);
            this.trackMin.MouseDown += new System.Windows.Forms.MouseEventHandler(this.trackMin_MouseUp);
            this.trackMin.MouseUp += new System.Windows.Forms.MouseEventHandler(this.trackMin_MouseUp);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.numMin);
            this.panel2.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.DownBack = null;
            this.panel2.Location = new System.Drawing.Point(557, 0);
            this.panel2.MouseBack = null;
            this.panel2.Name = "panel2";
            this.panel2.NormlBack = null;
            this.panel2.Size = new System.Drawing.Size(92, 25);
            this.panel2.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.BorderColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(63, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "分";
            // 
            // numMin
            // 
            this.numMin.ArrowColor = System.Drawing.Color.Black;
            this.numMin.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.numMin.BorderColor = System.Drawing.Color.Black;
            this.numMin.Location = new System.Drawing.Point(3, 2);
            this.numMin.Name = "numMin";
            this.numMin.Size = new System.Drawing.Size(56, 21);
            this.numMin.TabIndex = 0;
            this.numMin.ValueChanged += new System.EventHandler(this.numMin_ValueChanged);
            // 
            // skinGroupBox2
            // 
            this.skinGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.skinGroupBox2.BorderColor = System.Drawing.SystemColors.ActiveBorder;
            this.skinGroupBox2.Controls.Add(this.skinPanel3);
            this.skinGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skinGroupBox2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.skinGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.skinGroupBox2.Name = "skinGroupBox2";
            this.skinGroupBox2.RectBackColor = System.Drawing.Color.Transparent;
            this.skinGroupBox2.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.skinGroupBox2.Size = new System.Drawing.Size(231, 422);
            this.skinGroupBox2.TabIndex = 0;
            this.skinGroupBox2.TabStop = false;
            this.skinGroupBox2.Text = "成员信息编辑";
            this.skinGroupBox2.TitleBorderColor = System.Drawing.SystemColors.ActiveBorder;
            this.skinGroupBox2.TitleRectBackColor = System.Drawing.Color.White;
            this.skinGroupBox2.TitleRoundStyle = CCWin.SkinClass.RoundStyle.All;
            // 
            // skinPanel3
            // 
            this.skinPanel3.AutoScroll = true;
            this.skinPanel3.BackColor = System.Drawing.Color.Transparent;
            this.skinPanel3.Controls.Add(this.skinPanel7);
            this.skinPanel3.Controls.Add(this.skinPanel5);
            this.skinPanel3.Controls.Add(this.skinPanel4);
            this.skinPanel3.Controls.Add(this.skinPanel2);
            this.skinPanel3.Controls.Add(this.skinPanel1);
            this.skinPanel3.Controls.Add(this.skinPanel6);
            this.skinPanel3.Controls.Add(this.info);
            this.skinPanel3.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skinPanel3.DownBack = null;
            this.skinPanel3.Location = new System.Drawing.Point(3, 17);
            this.skinPanel3.MouseBack = null;
            this.skinPanel3.Name = "skinPanel3";
            this.skinPanel3.NormlBack = null;
            this.skinPanel3.Size = new System.Drawing.Size(225, 402);
            this.skinPanel3.TabIndex = 2;
            // 
            // skinPanel7
            // 
            this.skinPanel7.BackColor = System.Drawing.Color.Transparent;
            this.skinPanel7.Controls.Add(this.txtTag);
            this.skinPanel7.Controls.Add(this.skinLabel6);
            this.skinPanel7.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinPanel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.skinPanel7.DownBack = null;
            this.skinPanel7.Location = new System.Drawing.Point(0, 287);
            this.skinPanel7.MouseBack = null;
            this.skinPanel7.Name = "skinPanel7";
            this.skinPanel7.NormlBack = null;
            this.skinPanel7.Size = new System.Drawing.Size(225, 29);
            this.skinPanel7.TabIndex = 7;
            // 
            // txtTag
            // 
            this.txtTag.BackColor = System.Drawing.Color.Transparent;
            this.txtTag.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTag.DownBack = null;
            this.txtTag.Icon = null;
            this.txtTag.IconIsButton = false;
            this.txtTag.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.txtTag.IsPasswordChat = '\0';
            this.txtTag.IsSystemPasswordChar = false;
            this.txtTag.Lines = new string[0];
            this.txtTag.Location = new System.Drawing.Point(68, 0);
            this.txtTag.Margin = new System.Windows.Forms.Padding(0);
            this.txtTag.MaxLength = 32767;
            this.txtTag.MinimumSize = new System.Drawing.Size(28, 28);
            this.txtTag.MouseBack = null;
            this.txtTag.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.txtTag.Multiline = true;
            this.txtTag.Name = "txtTag";
            this.txtTag.NormlBack = null;
            this.txtTag.Padding = new System.Windows.Forms.Padding(5);
            this.txtTag.ReadOnly = false;
            this.txtTag.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtTag.Size = new System.Drawing.Size(157, 29);
            // 
            // 
            // 
            this.txtTag.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTag.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTag.SkinTxt.Font = new System.Drawing.Font("Microsoft YaHei", 9.75F);
            this.txtTag.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.txtTag.SkinTxt.Multiline = true;
            this.txtTag.SkinTxt.Name = "BaseText";
            this.txtTag.SkinTxt.Size = new System.Drawing.Size(147, 19);
            this.txtTag.SkinTxt.TabIndex = 0;
            this.txtTag.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.txtTag.SkinTxt.WaterText = "请输入备注";
            this.txtTag.SkinTxt.TextChanged += new System.EventHandler(this.skinTextBox1_SkinTxt_TextChanged_1);
            this.txtTag.TabIndex = 1;
            this.txtTag.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtTag.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.txtTag.WaterText = "请输入备注";
            this.txtTag.WordWrap = true;
            // 
            // skinLabel6
            // 
            this.skinLabel6.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel6.BorderColor = System.Drawing.Color.White;
            this.skinLabel6.Dock = System.Windows.Forms.DockStyle.Left;
            this.skinLabel6.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel6.Location = new System.Drawing.Point(0, 0);
            this.skinLabel6.Name = "skinLabel6";
            this.skinLabel6.Size = new System.Drawing.Size(68, 29);
            this.skinLabel6.TabIndex = 0;
            this.skinLabel6.Text = "备注：";
            this.skinLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // skinPanel5
            // 
            this.skinPanel5.BackColor = System.Drawing.Color.Transparent;
            this.skinPanel5.Controls.Add(this.txtArriveTime);
            this.skinPanel5.Controls.Add(this.skinLabel4);
            this.skinPanel5.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinPanel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.skinPanel5.DownBack = null;
            this.skinPanel5.Location = new System.Drawing.Point(0, 258);
            this.skinPanel5.MouseBack = null;
            this.skinPanel5.Name = "skinPanel5";
            this.skinPanel5.NormlBack = null;
            this.skinPanel5.Size = new System.Drawing.Size(225, 29);
            this.skinPanel5.TabIndex = 5;
            // 
            // txtArriveTime
            // 
            this.txtArriveTime.BackColor = System.Drawing.Color.Transparent;
            this.txtArriveTime.Controls.Add(this.btnSetArriveTime);
            this.txtArriveTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtArriveTime.DownBack = null;
            this.txtArriveTime.Icon = null;
            this.txtArriveTime.IconIsButton = false;
            this.txtArriveTime.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.txtArriveTime.IsPasswordChat = '\0';
            this.txtArriveTime.IsSystemPasswordChar = false;
            this.txtArriveTime.Lines = new string[0];
            this.txtArriveTime.Location = new System.Drawing.Point(68, 0);
            this.txtArriveTime.Margin = new System.Windows.Forms.Padding(0);
            this.txtArriveTime.MaxLength = 32767;
            this.txtArriveTime.MinimumSize = new System.Drawing.Size(28, 28);
            this.txtArriveTime.MouseBack = null;
            this.txtArriveTime.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.txtArriveTime.Multiline = true;
            this.txtArriveTime.Name = "txtArriveTime";
            this.txtArriveTime.NormlBack = null;
            this.txtArriveTime.Padding = new System.Windows.Forms.Padding(5);
            this.txtArriveTime.ReadOnly = true;
            this.txtArriveTime.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtArriveTime.Size = new System.Drawing.Size(157, 29);
            // 
            // 
            // 
            this.txtArriveTime.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtArriveTime.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtArriveTime.SkinTxt.Font = new System.Drawing.Font("Microsoft YaHei", 9.75F);
            this.txtArriveTime.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.txtArriveTime.SkinTxt.Multiline = true;
            this.txtArriveTime.SkinTxt.Name = "BaseText";
            this.txtArriveTime.SkinTxt.ReadOnly = true;
            this.txtArriveTime.SkinTxt.Size = new System.Drawing.Size(126, 19);
            this.txtArriveTime.SkinTxt.TabIndex = 0;
            this.txtArriveTime.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.txtArriveTime.SkinTxt.WaterText = "请单击右侧按钮设置当前时间";
            this.txtArriveTime.TabIndex = 1;
            this.txtArriveTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtArriveTime.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.txtArriveTime.WaterText = "请单击右侧按钮设置当前时间";
            this.txtArriveTime.WordWrap = true;
            // 
            // btnSetArriveTime
            // 
            this.btnSetArriveTime.BackColor = System.Drawing.Color.Transparent;
            this.btnSetArriveTime.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btnSetArriveTime.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSetArriveTime.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSetArriveTime.DownBack = null;
            this.btnSetArriveTime.Location = new System.Drawing.Point(131, 5);
            this.btnSetArriveTime.MouseBack = null;
            this.btnSetArriveTime.Name = "btnSetArriveTime";
            this.btnSetArriveTime.NormlBack = null;
            this.btnSetArriveTime.Size = new System.Drawing.Size(21, 19);
            this.btnSetArriveTime.TabIndex = 1;
            this.btnSetArriveTime.Text = "·";
            this.btnSetArriveTime.UseVisualStyleBackColor = false;
            this.btnSetArriveTime.Click += new System.EventHandler(this.btnSetArriveTime_Click);
            // 
            // skinLabel4
            // 
            this.skinLabel4.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel4.BorderColor = System.Drawing.Color.White;
            this.skinLabel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.skinLabel4.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel4.Location = new System.Drawing.Point(0, 0);
            this.skinLabel4.Name = "skinLabel4";
            this.skinLabel4.Size = new System.Drawing.Size(68, 29);
            this.skinLabel4.TabIndex = 0;
            this.skinLabel4.Text = "到达时间：";
            this.skinLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // skinPanel4
            // 
            this.skinPanel4.BackColor = System.Drawing.Color.Transparent;
            this.skinPanel4.Controls.Add(this.txtStartTime);
            this.skinPanel4.Controls.Add(this.skinLabel3);
            this.skinPanel4.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinPanel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.skinPanel4.DownBack = null;
            this.skinPanel4.Location = new System.Drawing.Point(0, 229);
            this.skinPanel4.MouseBack = null;
            this.skinPanel4.Name = "skinPanel4";
            this.skinPanel4.NormlBack = null;
            this.skinPanel4.Size = new System.Drawing.Size(225, 29);
            this.skinPanel4.TabIndex = 4;
            // 
            // txtStartTime
            // 
            this.txtStartTime.BackColor = System.Drawing.Color.Transparent;
            this.txtStartTime.Controls.Add(this.btnSetStartTime);
            this.txtStartTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtStartTime.DownBack = null;
            this.txtStartTime.Icon = null;
            this.txtStartTime.IconIsButton = false;
            this.txtStartTime.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.txtStartTime.IsPasswordChat = '\0';
            this.txtStartTime.IsSystemPasswordChar = false;
            this.txtStartTime.Lines = new string[0];
            this.txtStartTime.Location = new System.Drawing.Point(68, 0);
            this.txtStartTime.Margin = new System.Windows.Forms.Padding(0);
            this.txtStartTime.MaxLength = 32767;
            this.txtStartTime.MinimumSize = new System.Drawing.Size(28, 28);
            this.txtStartTime.MouseBack = null;
            this.txtStartTime.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.txtStartTime.Multiline = true;
            this.txtStartTime.Name = "txtStartTime";
            this.txtStartTime.NormlBack = null;
            this.txtStartTime.Padding = new System.Windows.Forms.Padding(5);
            this.txtStartTime.ReadOnly = true;
            this.txtStartTime.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtStartTime.Size = new System.Drawing.Size(157, 29);
            // 
            // 
            // 
            this.txtStartTime.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtStartTime.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtStartTime.SkinTxt.Font = new System.Drawing.Font("Microsoft YaHei", 9.75F);
            this.txtStartTime.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.txtStartTime.SkinTxt.Multiline = true;
            this.txtStartTime.SkinTxt.Name = "BaseText";
            this.txtStartTime.SkinTxt.ReadOnly = true;
            this.txtStartTime.SkinTxt.Size = new System.Drawing.Size(126, 19);
            this.txtStartTime.SkinTxt.TabIndex = 0;
            this.txtStartTime.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.txtStartTime.SkinTxt.WaterText = "请单击右侧按钮设置当前时间";
            this.txtStartTime.TabIndex = 1;
            this.txtStartTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtStartTime.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.txtStartTime.WaterText = "请单击右侧按钮设置当前时间";
            this.txtStartTime.WordWrap = true;
            // 
            // btnSetStartTime
            // 
            this.btnSetStartTime.BackColor = System.Drawing.Color.Transparent;
            this.btnSetStartTime.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btnSetStartTime.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSetStartTime.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSetStartTime.DownBack = null;
            this.btnSetStartTime.Location = new System.Drawing.Point(131, 5);
            this.btnSetStartTime.MouseBack = null;
            this.btnSetStartTime.Name = "btnSetStartTime";
            this.btnSetStartTime.NormlBack = null;
            this.btnSetStartTime.Size = new System.Drawing.Size(21, 19);
            this.btnSetStartTime.TabIndex = 1;
            this.btnSetStartTime.Text = "·";
            this.btnSetStartTime.UseVisualStyleBackColor = false;
            this.btnSetStartTime.Click += new System.EventHandler(this.btnSetStartTime_Click);
            // 
            // skinLabel3
            // 
            this.skinLabel3.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel3.BorderColor = System.Drawing.Color.White;
            this.skinLabel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.skinLabel3.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel3.Location = new System.Drawing.Point(0, 0);
            this.skinLabel3.Name = "skinLabel3";
            this.skinLabel3.Size = new System.Drawing.Size(68, 29);
            this.skinLabel3.TabIndex = 0;
            this.skinLabel3.Text = "出发时间：";
            this.skinLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // skinPanel2
            // 
            this.skinPanel2.BackColor = System.Drawing.Color.Transparent;
            this.skinPanel2.Controls.Add(this.txtName);
            this.skinPanel2.Controls.Add(this.skinLabel2);
            this.skinPanel2.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.skinPanel2.DownBack = null;
            this.skinPanel2.Location = new System.Drawing.Point(0, 200);
            this.skinPanel2.MouseBack = null;
            this.skinPanel2.Name = "skinPanel2";
            this.skinPanel2.NormlBack = null;
            this.skinPanel2.Size = new System.Drawing.Size(225, 29);
            this.skinPanel2.TabIndex = 3;
            // 
            // txtName
            // 
            this.txtName.BackColor = System.Drawing.Color.Transparent;
            this.txtName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtName.DownBack = null;
            this.txtName.Icon = null;
            this.txtName.IconIsButton = false;
            this.txtName.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.txtName.IsPasswordChat = '\0';
            this.txtName.IsSystemPasswordChar = false;
            this.txtName.Lines = new string[0];
            this.txtName.Location = new System.Drawing.Point(68, 0);
            this.txtName.Margin = new System.Windows.Forms.Padding(0);
            this.txtName.MaxLength = 32767;
            this.txtName.MinimumSize = new System.Drawing.Size(28, 28);
            this.txtName.MouseBack = null;
            this.txtName.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.txtName.Multiline = true;
            this.txtName.Name = "txtName";
            this.txtName.NormlBack = null;
            this.txtName.Padding = new System.Windows.Forms.Padding(5);
            this.txtName.ReadOnly = false;
            this.txtName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtName.Size = new System.Drawing.Size(157, 29);
            // 
            // 
            // 
            this.txtName.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtName.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtName.SkinTxt.Font = new System.Drawing.Font("Microsoft YaHei", 9.75F);
            this.txtName.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.txtName.SkinTxt.Multiline = true;
            this.txtName.SkinTxt.Name = "BaseText";
            this.txtName.SkinTxt.Size = new System.Drawing.Size(147, 19);
            this.txtName.SkinTxt.TabIndex = 0;
            this.txtName.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.txtName.SkinTxt.WaterText = "请输入姓名";
            this.txtName.SkinTxt.TextChanged += new System.EventHandler(this.txtName_SkinTxt_TextChanged);
            this.txtName.TabIndex = 1;
            this.txtName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtName.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.txtName.WaterText = "请输入姓名";
            this.txtName.WordWrap = true;
            // 
            // skinLabel2
            // 
            this.skinLabel2.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel2.BorderColor = System.Drawing.Color.White;
            this.skinLabel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.skinLabel2.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel2.Location = new System.Drawing.Point(0, 0);
            this.skinLabel2.Name = "skinLabel2";
            this.skinLabel2.Size = new System.Drawing.Size(68, 29);
            this.skinLabel2.TabIndex = 0;
            this.skinLabel2.Text = "姓名：";
            this.skinLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // skinPanel1
            // 
            this.skinPanel1.BackColor = System.Drawing.Color.Transparent;
            this.skinPanel1.Controls.Add(this.txtID);
            this.skinPanel1.Controls.Add(this.skinLabel1);
            this.skinPanel1.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.skinPanel1.DownBack = null;
            this.skinPanel1.Location = new System.Drawing.Point(0, 171);
            this.skinPanel1.MouseBack = null;
            this.skinPanel1.Name = "skinPanel1";
            this.skinPanel1.NormlBack = null;
            this.skinPanel1.Size = new System.Drawing.Size(225, 29);
            this.skinPanel1.TabIndex = 2;
            // 
            // txtID
            // 
            this.txtID.BackColor = System.Drawing.Color.Transparent;
            this.txtID.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtID.DownBack = null;
            this.txtID.Icon = null;
            this.txtID.IconIsButton = false;
            this.txtID.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.txtID.IsPasswordChat = '\0';
            this.txtID.IsSystemPasswordChar = false;
            this.txtID.Lines = new string[0];
            this.txtID.Location = new System.Drawing.Point(68, 0);
            this.txtID.Margin = new System.Windows.Forms.Padding(0);
            this.txtID.MaxLength = 32767;
            this.txtID.MinimumSize = new System.Drawing.Size(28, 28);
            this.txtID.MouseBack = null;
            this.txtID.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.txtID.Multiline = true;
            this.txtID.Name = "txtID";
            this.txtID.NormlBack = null;
            this.txtID.Padding = new System.Windows.Forms.Padding(5);
            this.txtID.ReadOnly = false;
            this.txtID.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtID.Size = new System.Drawing.Size(157, 29);
            // 
            // 
            // 
            this.txtID.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtID.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtID.SkinTxt.Font = new System.Drawing.Font("Microsoft YaHei", 9.75F);
            this.txtID.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.txtID.SkinTxt.Multiline = true;
            this.txtID.SkinTxt.Name = "BaseText";
            this.txtID.SkinTxt.Size = new System.Drawing.Size(147, 19);
            this.txtID.SkinTxt.TabIndex = 0;
            this.txtID.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.txtID.SkinTxt.WaterText = "请输入号码";
            this.txtID.SkinTxt.TextChanged += new System.EventHandler(this.txtID_SkinTxt_TextChanged);
            this.txtID.TabIndex = 1;
            this.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtID.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.txtID.WaterText = "请输入号码";
            this.txtID.WordWrap = true;
            // 
            // skinLabel1
            // 
            this.skinLabel1.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel1.BorderColor = System.Drawing.Color.White;
            this.skinLabel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.skinLabel1.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel1.Location = new System.Drawing.Point(0, 0);
            this.skinLabel1.Name = "skinLabel1";
            this.skinLabel1.Size = new System.Drawing.Size(68, 29);
            this.skinLabel1.TabIndex = 0;
            this.skinLabel1.Text = "号码：";
            this.skinLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // skinPanel6
            // 
            this.skinPanel6.BackColor = System.Drawing.Color.Transparent;
            this.skinPanel6.Controls.Add(this.txtLineID);
            this.skinPanel6.Controls.Add(this.skinLabel5);
            this.skinPanel6.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinPanel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.skinPanel6.DownBack = null;
            this.skinPanel6.Location = new System.Drawing.Point(0, 142);
            this.skinPanel6.MouseBack = null;
            this.skinPanel6.Name = "skinPanel6";
            this.skinPanel6.NormlBack = null;
            this.skinPanel6.Size = new System.Drawing.Size(225, 29);
            this.skinPanel6.TabIndex = 6;
            // 
            // txtLineID
            // 
            this.txtLineID.BackColor = System.Drawing.Color.Transparent;
            this.txtLineID.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLineID.DownBack = null;
            this.txtLineID.Icon = null;
            this.txtLineID.IconIsButton = false;
            this.txtLineID.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.txtLineID.IsPasswordChat = '\0';
            this.txtLineID.IsSystemPasswordChar = false;
            this.txtLineID.Lines = new string[0];
            this.txtLineID.Location = new System.Drawing.Point(68, 0);
            this.txtLineID.Margin = new System.Windows.Forms.Padding(0);
            this.txtLineID.MaxLength = 32767;
            this.txtLineID.MinimumSize = new System.Drawing.Size(28, 28);
            this.txtLineID.MouseBack = null;
            this.txtLineID.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.txtLineID.Multiline = true;
            this.txtLineID.Name = "txtLineID";
            this.txtLineID.NormlBack = null;
            this.txtLineID.Padding = new System.Windows.Forms.Padding(5);
            this.txtLineID.ReadOnly = false;
            this.txtLineID.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtLineID.Size = new System.Drawing.Size(157, 29);
            // 
            // 
            // 
            this.txtLineID.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtLineID.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLineID.SkinTxt.Font = new System.Drawing.Font("Microsoft YaHei", 9.75F);
            this.txtLineID.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.txtLineID.SkinTxt.Multiline = true;
            this.txtLineID.SkinTxt.Name = "BaseText";
            this.txtLineID.SkinTxt.Size = new System.Drawing.Size(147, 19);
            this.txtLineID.SkinTxt.TabIndex = 0;
            this.txtLineID.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.txtLineID.SkinTxt.WaterText = "请输入道号";
            this.txtLineID.SkinTxt.TextChanged += new System.EventHandler(this.skinTextBox1_SkinTxt_TextChanged);
            this.txtLineID.TabIndex = 1;
            this.txtLineID.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtLineID.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.txtLineID.WaterText = "请输入道号";
            this.txtLineID.WordWrap = true;
            // 
            // skinLabel5
            // 
            this.skinLabel5.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel5.BorderColor = System.Drawing.Color.White;
            this.skinLabel5.Dock = System.Windows.Forms.DockStyle.Left;
            this.skinLabel5.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel5.Location = new System.Drawing.Point(0, 0);
            this.skinLabel5.Name = "skinLabel5";
            this.skinLabel5.Size = new System.Drawing.Size(68, 29);
            this.skinLabel5.TabIndex = 0;
            this.skinLabel5.Text = "道号：";
            this.skinLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // skinGroupBox1
            // 
            this.skinGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.skinGroupBox1.BorderColor = System.Drawing.SystemColors.ActiveBorder;
            this.skinGroupBox1.Controls.Add(this.lstviewMembers);
            this.skinGroupBox1.Controls.Add(this.skinToolStrip1);
            this.skinGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skinGroupBox1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.skinGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.skinGroupBox1.MinimumSize = new System.Drawing.Size(100, 100);
            this.skinGroupBox1.Name = "skinGroupBox1";
            this.skinGroupBox1.RectBackColor = System.Drawing.Color.Transparent;
            this.skinGroupBox1.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.skinGroupBox1.Size = new System.Drawing.Size(896, 197);
            this.skinGroupBox1.TabIndex = 0;
            this.skinGroupBox1.TabStop = false;
            this.skinGroupBox1.Text = "计时面板";
            this.skinGroupBox1.TitleBorderColor = System.Drawing.SystemColors.ActiveBorder;
            this.skinGroupBox1.TitleRectBackColor = System.Drawing.Color.White;
            this.skinGroupBox1.TitleRoundStyle = CCWin.SkinClass.RoundStyle.All;
            // 
            // skinToolStrip1
            // 
            this.skinToolStrip1.Arrow = System.Drawing.Color.Black;
            this.skinToolStrip1.Back = System.Drawing.Color.White;
            this.skinToolStrip1.BackRadius = 4;
            this.skinToolStrip1.BackRectangle = new System.Drawing.Rectangle(10, 10, 10, 10);
            this.skinToolStrip1.Base = System.Drawing.Color.Transparent;
            this.skinToolStrip1.BaseFore = System.Drawing.Color.Black;
            this.skinToolStrip1.BaseForeAnamorphosis = false;
            this.skinToolStrip1.BaseForeAnamorphosisBorder = 4;
            this.skinToolStrip1.BaseForeAnamorphosisColor = System.Drawing.Color.White;
            this.skinToolStrip1.BaseForeOffset = new System.Drawing.Point(0, 0);
            this.skinToolStrip1.BaseHoverFore = System.Drawing.Color.Black;
            this.skinToolStrip1.BaseItemAnamorphosis = true;
            this.skinToolStrip1.BaseItemBorder = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.skinToolStrip1.BaseItemBorderShow = true;
            this.skinToolStrip1.BaseItemDown = ((System.Drawing.Image)(resources.GetObject("skinToolStrip1.BaseItemDown")));
            this.skinToolStrip1.BaseItemHover = System.Drawing.Color.Black;
            this.skinToolStrip1.BaseItemMouse = ((System.Drawing.Image)(resources.GetObject("skinToolStrip1.BaseItemMouse")));
            this.skinToolStrip1.BaseItemNorml = null;
            this.skinToolStrip1.BaseItemPressed = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.skinToolStrip1.BaseItemRadius = 4;
            this.skinToolStrip1.BaseItemRadiusStyle = CCWin.SkinClass.RoundStyle.All;
            this.skinToolStrip1.BaseItemSplitter = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.skinToolStrip1.BindTabControl = null;
            this.skinToolStrip1.DropDownImageSeparator = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(197)))), ((int)(((byte)(197)))));
            this.skinToolStrip1.Fore = System.Drawing.Color.Black;
            this.skinToolStrip1.GripMargin = new System.Windows.Forms.Padding(2, 2, 4, 2);
            this.skinToolStrip1.HoverFore = System.Drawing.Color.White;
            this.skinToolStrip1.ItemAnamorphosis = true;
            this.skinToolStrip1.ItemBorder = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.skinToolStrip1.ItemBorderShow = true;
            this.skinToolStrip1.ItemHover = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.skinToolStrip1.ItemPressed = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.skinToolStrip1.ItemRadius = 4;
            this.skinToolStrip1.ItemRadiusStyle = CCWin.SkinClass.RoundStyle.All;
            this.skinToolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel3,
            this.tbtnAddMember,
            this.tbtnSelAll,
            this.tbtnUnselAll,
            this.tbtnReverseSel,
            this.tbtnRemoveSel,
            this.tbtnPrint,
            this.tbtnSave});
            this.skinToolStrip1.Location = new System.Drawing.Point(3, 17);
            this.skinToolStrip1.Name = "skinToolStrip1";
            this.skinToolStrip1.RadiusStyle = CCWin.SkinClass.RoundStyle.All;
            this.skinToolStrip1.Size = new System.Drawing.Size(890, 25);
            this.skinToolStrip1.SkinAllColor = true;
            this.skinToolStrip1.TabIndex = 1;
            this.skinToolStrip1.Text = "skinToolStrip1";
            this.skinToolStrip1.TitleAnamorphosis = true;
            this.skinToolStrip1.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(228)))), ((int)(((byte)(236)))));
            this.skinToolStrip1.TitleRadius = 4;
            this.skinToolStrip1.TitleRadiusStyle = CCWin.SkinClass.RoundStyle.All;
            // 
            // toolStripLabel3
            // 
            this.toolStripLabel3.Name = "toolStripLabel3";
            this.toolStripLabel3.Size = new System.Drawing.Size(68, 22);
            this.toolStripLabel3.Text = "成员管理：";
            // 
            // tbtnAddMember
            // 
            this.tbtnAddMember.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnAddMember.Image = ((System.Drawing.Image)(resources.GetObject("tbtnAddMember.Image")));
            this.tbtnAddMember.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnAddMember.Name = "tbtnAddMember";
            this.tbtnAddMember.Size = new System.Drawing.Size(60, 22);
            this.tbtnAddMember.Text = "添加成员";
            this.tbtnAddMember.Click += new System.EventHandler(this.tbtnAddMember_Click);
            // 
            // tbtnSelAll
            // 
            this.tbtnSelAll.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnSelAll.Image = ((System.Drawing.Image)(resources.GetObject("tbtnSelAll.Image")));
            this.tbtnSelAll.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnSelAll.Name = "tbtnSelAll";
            this.tbtnSelAll.Size = new System.Drawing.Size(36, 22);
            this.tbtnSelAll.Text = "全选";
            this.tbtnSelAll.Click += new System.EventHandler(this.tbtnSelAll_Click);
            // 
            // tbtnUnselAll
            // 
            this.tbtnUnselAll.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnUnselAll.Image = ((System.Drawing.Image)(resources.GetObject("tbtnUnselAll.Image")));
            this.tbtnUnselAll.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnUnselAll.Name = "tbtnUnselAll";
            this.tbtnUnselAll.Size = new System.Drawing.Size(48, 22);
            this.tbtnUnselAll.Text = "全不选";
            this.tbtnUnselAll.Click += new System.EventHandler(this.tbtnUnselAll_Click);
            // 
            // tbtnReverseSel
            // 
            this.tbtnReverseSel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnReverseSel.Image = ((System.Drawing.Image)(resources.GetObject("tbtnReverseSel.Image")));
            this.tbtnReverseSel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnReverseSel.Name = "tbtnReverseSel";
            this.tbtnReverseSel.Size = new System.Drawing.Size(36, 22);
            this.tbtnReverseSel.Text = "反选";
            this.tbtnReverseSel.Click += new System.EventHandler(this.tbtnReverseSel_Click);
            // 
            // tbtnRemoveSel
            // 
            this.tbtnRemoveSel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnRemoveSel.Image = ((System.Drawing.Image)(resources.GetObject("tbtnRemoveSel.Image")));
            this.tbtnRemoveSel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnRemoveSel.Name = "tbtnRemoveSel";
            this.tbtnRemoveSel.Size = new System.Drawing.Size(60, 22);
            this.tbtnRemoveSel.Text = "移除选中";
            this.tbtnRemoveSel.Click += new System.EventHandler(this.tbtnRemoveSel_Click);
            // 
            // tbtnPrint
            // 
            this.tbtnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnPrint.Image = ((System.Drawing.Image)(resources.GetObject("tbtnPrint.Image")));
            this.tbtnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnPrint.Name = "tbtnPrint";
            this.tbtnPrint.Size = new System.Drawing.Size(36, 22);
            this.tbtnPrint.Text = "打印";
            this.tbtnPrint.Click += new System.EventHandler(this.tbtnPrint_Click);
            // 
            // tbtnSave
            // 
            this.tbtnSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnSave.Image = ((System.Drawing.Image)(resources.GetObject("tbtnSave.Image")));
            this.tbtnSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnSave.Name = "tbtnSave";
            this.tbtnSave.Size = new System.Drawing.Size(36, 22);
            this.tbtnSave.Text = "保存";
            this.tbtnSave.Click += new System.EventHandler(this.tbtnSave_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tlabelInfo,
            this.tlabelStatu,
            this.tlabelTime});
            this.statusStrip1.Location = new System.Drawing.Point(0, 0);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(896, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tlabelInfo
            // 
            this.tlabelInfo.Name = "tlabelInfo";
            this.tlabelInfo.Size = new System.Drawing.Size(817, 17);
            this.tlabelInfo.Spring = true;
            this.tlabelInfo.Text = "信息";
            this.tlabelInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tlabelStatu
            // 
            this.tlabelStatu.Name = "tlabelStatu";
            this.tlabelStatu.Size = new System.Drawing.Size(32, 17);
            this.tlabelStatu.Text = "状态";
            // 
            // tlabelTime
            // 
            this.tlabelTime.Name = "tlabelTime";
            this.tlabelTime.Size = new System.Drawing.Size(32, 17);
            this.tlabelTime.Text = "时间";
            // 
            // toolsRecSettting
            // 
            this.toolsRecSettting.Arrow = System.Drawing.Color.Black;
            this.toolsRecSettting.Back = System.Drawing.Color.White;
            this.toolsRecSettting.BackRadius = 4;
            this.toolsRecSettting.BackRectangle = new System.Drawing.Rectangle(10, 10, 10, 10);
            this.toolsRecSettting.Base = System.Drawing.Color.Transparent;
            this.toolsRecSettting.BaseFore = System.Drawing.Color.Black;
            this.toolsRecSettting.BaseForeAnamorphosis = false;
            this.toolsRecSettting.BaseForeAnamorphosisBorder = 4;
            this.toolsRecSettting.BaseForeAnamorphosisColor = System.Drawing.Color.White;
            this.toolsRecSettting.BaseForeOffset = new System.Drawing.Point(0, 0);
            this.toolsRecSettting.BaseHoverFore = System.Drawing.Color.Black;
            this.toolsRecSettting.BaseItemAnamorphosis = true;
            this.toolsRecSettting.BaseItemBorder = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.toolsRecSettting.BaseItemBorderShow = true;
            this.toolsRecSettting.BaseItemDown = ((System.Drawing.Image)(resources.GetObject("toolsRecSettting.BaseItemDown")));
            this.toolsRecSettting.BaseItemHover = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.toolsRecSettting.BaseItemMouse = ((System.Drawing.Image)(resources.GetObject("toolsRecSettting.BaseItemMouse")));
            this.toolsRecSettting.BaseItemNorml = null;
            this.toolsRecSettting.BaseItemPressed = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.toolsRecSettting.BaseItemRadius = 4;
            this.toolsRecSettting.BaseItemRadiusStyle = CCWin.SkinClass.RoundStyle.All;
            this.toolsRecSettting.BaseItemSplitter = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.toolsRecSettting.BindTabControl = null;
            this.toolsRecSettting.Dock = System.Windows.Forms.DockStyle.None;
            this.toolsRecSettting.DropDownImageSeparator = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(197)))), ((int)(((byte)(197)))));
            this.toolsRecSettting.Fore = System.Drawing.Color.Black;
            this.toolsRecSettting.GripMargin = new System.Windows.Forms.Padding(2, 2, 4, 2);
            this.toolsRecSettting.HoverFore = System.Drawing.Color.White;
            this.toolsRecSettting.ItemAnamorphosis = true;
            this.toolsRecSettting.ItemBorder = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.toolsRecSettting.ItemBorderShow = true;
            this.toolsRecSettting.ItemHover = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.toolsRecSettting.ItemPressed = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.toolsRecSettting.ItemRadius = 4;
            this.toolsRecSettting.ItemRadiusStyle = CCWin.SkinClass.RoundStyle.All;
            this.toolsRecSettting.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.tcomRecFile,
            this.tbtnSelRec});
            this.toolsRecSettting.Location = new System.Drawing.Point(3, 25);
            this.toolsRecSettting.Name = "toolsRecSettting";
            this.toolsRecSettting.RadiusStyle = CCWin.SkinClass.RoundStyle.All;
            this.toolsRecSettting.Size = new System.Drawing.Size(320, 25);
            this.toolsRecSettting.SkinAllColor = true;
            this.toolsRecSettting.TabIndex = 2;
            this.toolsRecSettting.Text = "toolStrip1";
            this.toolsRecSettting.TitleAnamorphosis = true;
            this.toolsRecSettting.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(228)))), ((int)(((byte)(236)))));
            this.toolsRecSettting.TitleRadius = 4;
            this.toolsRecSettting.TitleRadiusStyle = CCWin.SkinClass.RoundStyle.All;
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(68, 22);
            this.toolStripLabel1.Text = "录像文件：";
            // 
            // tcomRecFile
            // 
            this.tcomRecFile.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tcomRecFile.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.tcomRecFile.Name = "tcomRecFile";
            this.tcomRecFile.Size = new System.Drawing.Size(200, 25);
            // 
            // tbtnSelRec
            // 
            this.tbtnSelRec.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnSelRec.Image = ((System.Drawing.Image)(resources.GetObject("tbtnSelRec.Image")));
            this.tbtnSelRec.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnSelRec.Name = "tbtnSelRec";
            this.tbtnSelRec.Size = new System.Drawing.Size(36, 22);
            this.tbtnSelRec.Text = "选择";
            this.tbtnSelRec.Click += new System.EventHandler(this.tbtnSelRec_Click);
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.BottomToolStripPanel
            // 
            this.toolStripContainer1.BottomToolStripPanel.Controls.Add(this.statusStrip1);
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.AutoScroll = true;
            this.toolStripContainer1.ContentPanel.Controls.Add(this.splitContainer1);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(896, 623);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.Location = new System.Drawing.Point(4, 28);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(896, 695);
            this.toolStripContainer1.TabIndex = 3;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.BackColor = System.Drawing.Color.Transparent;
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.toolsPlaySetting);
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.toolsRecSettting);
            // 
            // toolsPlaySetting
            // 
            this.toolsPlaySetting.Arrow = System.Drawing.Color.Black;
            this.toolsPlaySetting.Back = System.Drawing.Color.White;
            this.toolsPlaySetting.BackRadius = 4;
            this.toolsPlaySetting.BackRectangle = new System.Drawing.Rectangle(10, 10, 10, 10);
            this.toolsPlaySetting.Base = System.Drawing.Color.Transparent;
            this.toolsPlaySetting.BaseFore = System.Drawing.Color.Black;
            this.toolsPlaySetting.BaseForeAnamorphosis = false;
            this.toolsPlaySetting.BaseForeAnamorphosisBorder = 4;
            this.toolsPlaySetting.BaseForeAnamorphosisColor = System.Drawing.Color.White;
            this.toolsPlaySetting.BaseForeOffset = new System.Drawing.Point(0, 0);
            this.toolsPlaySetting.BaseHoverFore = System.Drawing.Color.Black;
            this.toolsPlaySetting.BaseItemAnamorphosis = true;
            this.toolsPlaySetting.BaseItemBorder = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.toolsPlaySetting.BaseItemBorderShow = true;
            this.toolsPlaySetting.BaseItemDown = ((System.Drawing.Image)(resources.GetObject("toolsPlaySetting.BaseItemDown")));
            this.toolsPlaySetting.BaseItemHover = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.toolsPlaySetting.BaseItemMouse = ((System.Drawing.Image)(resources.GetObject("toolsPlaySetting.BaseItemMouse")));
            this.toolsPlaySetting.BaseItemNorml = null;
            this.toolsPlaySetting.BaseItemPressed = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.toolsPlaySetting.BaseItemRadius = 4;
            this.toolsPlaySetting.BaseItemRadiusStyle = CCWin.SkinClass.RoundStyle.All;
            this.toolsPlaySetting.BaseItemSplitter = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.toolsPlaySetting.BindTabControl = null;
            this.toolsPlaySetting.Dock = System.Windows.Forms.DockStyle.None;
            this.toolsPlaySetting.DropDownImageSeparator = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(197)))), ((int)(((byte)(197)))));
            this.toolsPlaySetting.Enabled = false;
            this.toolsPlaySetting.Fore = System.Drawing.Color.Black;
            this.toolsPlaySetting.GripMargin = new System.Windows.Forms.Padding(2, 2, 4, 2);
            this.toolsPlaySetting.HoverFore = System.Drawing.Color.White;
            this.toolsPlaySetting.ItemAnamorphosis = true;
            this.toolsPlaySetting.ItemBorder = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.toolsPlaySetting.ItemBorderShow = true;
            this.toolsPlaySetting.ItemHover = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.toolsPlaySetting.ItemPressed = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.toolsPlaySetting.ItemRadius = 4;
            this.toolsPlaySetting.ItemRadiusStyle = CCWin.SkinClass.RoundStyle.All;
            this.toolsPlaySetting.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel2,
            this.tbtnPlay,
            this.tbtnStop,
            this.tbtnFastForward,
            this.tbtnFastBackward,
            this.tbtnTrim});
            this.toolsPlaySetting.Location = new System.Drawing.Point(3, 0);
            this.toolsPlaySetting.Name = "toolsPlaySetting";
            this.toolsPlaySetting.RadiusStyle = CCWin.SkinClass.RoundStyle.All;
            this.toolsPlaySetting.Size = new System.Drawing.Size(293, 25);
            this.toolsPlaySetting.SkinAllColor = true;
            this.toolsPlaySetting.TabIndex = 3;
            this.toolsPlaySetting.TitleAnamorphosis = true;
            this.toolsPlaySetting.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(228)))), ((int)(((byte)(236)))));
            this.toolsPlaySetting.TitleRadius = 4;
            this.toolsPlaySetting.TitleRadiusStyle = CCWin.SkinClass.RoundStyle.All;
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(68, 22);
            this.toolStripLabel2.Text = "播放控制：";
            // 
            // tbtnPlay
            // 
            this.tbtnPlay.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnPlay.Image = ((System.Drawing.Image)(resources.GetObject("tbtnPlay.Image")));
            this.tbtnPlay.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnPlay.Name = "tbtnPlay";
            this.tbtnPlay.Size = new System.Drawing.Size(36, 22);
            this.tbtnPlay.Text = "开始";
            this.tbtnPlay.Click += new System.EventHandler(this.tbtnPlay_Click);
            // 
            // tbtnStop
            // 
            this.tbtnStop.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnStop.Enabled = false;
            this.tbtnStop.Image = ((System.Drawing.Image)(resources.GetObject("tbtnStop.Image")));
            this.tbtnStop.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnStop.Name = "tbtnStop";
            this.tbtnStop.Size = new System.Drawing.Size(36, 22);
            this.tbtnStop.Text = "停止";
            this.tbtnStop.Click += new System.EventHandler(this.tbtnStop_Click);
            // 
            // tbtnFastForward
            // 
            this.tbtnFastForward.CheckOnClick = true;
            this.tbtnFastForward.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnFastForward.Enabled = false;
            this.tbtnFastForward.Image = ((System.Drawing.Image)(resources.GetObject("tbtnFastForward.Image")));
            this.tbtnFastForward.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnFastForward.Name = "tbtnFastForward";
            this.tbtnFastForward.Size = new System.Drawing.Size(36, 22);
            this.tbtnFastForward.Text = "快进";
            this.tbtnFastForward.CheckedChanged += new System.EventHandler(this.tbtnFastForward_CheckedChanged);
            this.tbtnFastForward.Click += new System.EventHandler(this.tbtnFastForward_Click);
            // 
            // tbtnFastBackward
            // 
            this.tbtnFastBackward.CheckOnClick = true;
            this.tbtnFastBackward.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnFastBackward.Enabled = false;
            this.tbtnFastBackward.Image = ((System.Drawing.Image)(resources.GetObject("tbtnFastBackward.Image")));
            this.tbtnFastBackward.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnFastBackward.Name = "tbtnFastBackward";
            this.tbtnFastBackward.Size = new System.Drawing.Size(36, 22);
            this.tbtnFastBackward.Text = "快退";
            this.tbtnFastBackward.Click += new System.EventHandler(this.tbtnFastBackward_Click);
            // 
            // tbtnTrim
            // 
            this.tbtnTrim.CheckOnClick = true;
            this.tbtnTrim.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tbtnTrim.Enabled = false;
            this.tbtnTrim.Image = ((System.Drawing.Image)(resources.GetObject("tbtnTrim.Image")));
            this.tbtnTrim.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtnTrim.Name = "tbtnTrim";
            this.tbtnTrim.Size = new System.Drawing.Size(36, 22);
            this.tbtnTrim.Text = "微调";
            this.tbtnTrim.CheckedChanged += new System.EventHandler(this.tbtnTrim_CheckedChanged);
            // 
            // timerPlayTime
            // 
            this.timerPlayTime.Interval = 10;
            this.timerPlayTime.Tick += new System.EventHandler(this.timerPlayTime_Tick);
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.DefaultExt = "csv";
            this.saveFileDialog.Filter = "表格|*.csv";
            this.saveFileDialog.Title = "保存结果";
            this.saveFileDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.saveFileDialog_FileOk);
            // 
            // timerTimeSet
            // 
            this.timerTimeSet.Interval = 200;
            this.timerTimeSet.Tick += new System.EventHandler(this.timerTimeSet_Tick);
            // 
            // info
            // 
            this.info.BackColor = System.Drawing.SystemColors.Control;
            this.info.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.info.Dock = System.Windows.Forms.DockStyle.Top;
            this.info.Location = new System.Drawing.Point(0, 0);
            this.info.Name = "info";
            this.info.Size = new System.Drawing.Size(225, 142);
            this.info.TabIndex = 8;
            // 
            // lstviewMembers
            // 
            this.lstviewMembers.BackColor = System.Drawing.SystemColors.Control;
            this.lstviewMembers.BodyFont = new System.Drawing.Font("SimSun", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lstviewMembers.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colLineID,
            this.colName,
            this.colID,
            this.colStartTime,
            this.colArriveTime,
            this.colCost,
            this.colScore,
            this.colTag});
            this.lstviewMembers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstviewMembers.Font = new System.Drawing.Font("SimSun", 9F);
            this.lstviewMembers.FullRowSelect = true;
            this.lstviewMembers.GridLines = true;
            this.lstviewMembers.HeaderFont = new System.Drawing.Font("SimSun", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lstviewMembers.HideSelection = false;
            this.lstviewMembers.IsAlwaysPrintHeader = true;
            this.lstviewMembers.IsPreview = true;
            this.lstviewMembers.LineSpace = 0;
            this.lstviewMembers.Location = new System.Drawing.Point(3, 42);
            this.lstviewMembers.Name = "lstviewMembers";
            this.lstviewMembers.PrintHeaderString = "";
            this.lstviewMembers.Size = new System.Drawing.Size(890, 152);
            this.lstviewMembers.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.lstviewMembers.TabIndex = 0;
            this.lstviewMembers.TailFont = null;
            this.lstviewMembers.UseCompatibleStateImageBehavior = false;
            this.lstviewMembers.View = System.Windows.Forms.View.Details;
            this.lstviewMembers.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lstviewMembers_ColumnClick);
            this.lstviewMembers.SelectedIndexChanged += new System.EventHandler(this.lstviewMembers_SelectedIndexChanged);
            // 
            // colLineID
            // 
            this.colLineID.Text = "道次";
            // 
            // colName
            // 
            this.colName.Text = "姓名";
            // 
            // colID
            // 
            this.colID.Text = "号码";
            // 
            // colStartTime
            // 
            this.colStartTime.Text = "出发时间";
            this.colStartTime.Width = 100;
            // 
            // colArriveTime
            // 
            this.colArriveTime.Text = "到达时间";
            this.colArriveTime.Width = 100;
            // 
            // colCost
            // 
            this.colCost.Text = "成绩";
            this.colCost.Width = 100;
            // 
            // colScore
            // 
            this.colScore.Text = "名次";
            // 
            // colTag
            // 
            this.colTag.Text = "备注";
            this.colTag.Width = 300;
            // 
            // frmCount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(904, 727);
            this.Controls.Add(this.toolStripContainer1);
            this.InnerBorderColor = System.Drawing.Color.Black;
            this.Name = "frmCount";
            this.ShowSystemMenu = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "计时窗口";
            this.Activated += new System.EventHandler(this.frmCount_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmCount_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmCount_FormClosed);
            this.Load += new System.EventHandler(this.frmCount_Load);
            this.Shown += new System.EventHandler(this.frmCount_Shown);
            this.Leave += new System.EventHandler(this.frmCount_Leave);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.skinSplitContainer2.Panel1.ResumeLayout(false);
            this.skinSplitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.skinSplitContainer2)).EndInit();
            this.skinSplitContainer2.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.playerRec)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackLoc)).EndInit();
            this.gpTrim.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackMillionSec)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numMillionSec)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackSec)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSec)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackMin)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numMin)).EndInit();
            this.skinGroupBox2.ResumeLayout(false);
            this.skinPanel3.ResumeLayout(false);
            this.skinPanel7.ResumeLayout(false);
            this.skinPanel5.ResumeLayout(false);
            this.txtArriveTime.ResumeLayout(false);
            this.txtArriveTime.PerformLayout();
            this.skinPanel4.ResumeLayout(false);
            this.txtStartTime.ResumeLayout(false);
            this.txtStartTime.PerformLayout();
            this.skinPanel2.ResumeLayout(false);
            this.skinPanel1.ResumeLayout(false);
            this.skinPanel6.ResumeLayout(false);
            this.skinGroupBox1.ResumeLayout(false);
            this.skinGroupBox1.PerformLayout();
            this.skinToolStrip1.ResumeLayout(false);
            this.skinToolStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.toolsRecSettting.ResumeLayout(false);
            this.toolsRecSettting.PerformLayout();
            this.toolStripContainer1.BottomToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.BottomToolStripPanel.PerformLayout();
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.toolsPlaySetting.ResumeLayout(false);
            this.toolsPlaySetting.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private CCWin.SkinControl.SkinSplitContainer splitContainer1;
        private CCWin.SkinControl.SkinGroupBox groupBox2;
        private CCWin.SkinControl.SkinGroupBox gpTrim;
        private CCWin.SkinControl.SkinPanel panel5;
        private CCWin.SkinControl.SkinPanel panel6;
        private CCWin.SkinControl.SkinLabel label3;
        private CCWin.SkinControl.SkinNumericUpDown numMillionSec;
        private CCWin.SkinControl.SkinPanel panel3;
        private CCWin.SkinControl.SkinPanel panel4;
        private CCWin.SkinControl.SkinLabel label2;
        private CCWin.SkinControl.SkinNumericUpDown numSec;
        private CCWin.SkinControl.SkinPanel panel1;
        private CCWin.SkinControl.SkinPanel panel2;
        private CCWin.SkinControl.SkinLabel label1;
        private CCWin.SkinControl.SkinNumericUpDown numMin;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private CCWin.SkinControl.SkinToolStrip toolsRecSettting;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private CCWin.SkinControl.SkinGroupBox groupBox1;
        private System.Windows.Forms.ToolStripStatusLabel tlabelInfo;
        private System.Windows.Forms.ToolStripStatusLabel tlabelTime;
        private System.Windows.Forms.ToolStripComboBox tcomRecFile;
        private System.Windows.Forms.ToolStripButton tbtnSelRec;
        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private CCWin.SkinControl.SkinToolStrip toolsPlaySetting;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripButton tbtnPlay;
        private System.Windows.Forms.ToolStripButton tbtnStop;
        private System.Windows.Forms.ToolStripButton tbtnTrim;
        private AxWMPLib.AxWindowsMediaPlayer playerRec;
        private System.Windows.Forms.ToolStripStatusLabel tlabelStatu;
        private System.Windows.Forms.Timer timerPlayTime;
        private System.Windows.Forms.ToolStripButton tbtnFastForward;
        private System.Windows.Forms.ToolStripButton tbtnFastBackward;
        private CCWin.SkinControl.SkinTrackBar trackLoc;
        private CCWin.SkinControl.SkinTrackBar trackMillionSec;
        private CCWin.SkinControl.SkinTrackBar trackSec;
        private CCWin.SkinControl.SkinTrackBar trackMin;
        private CCWin.SkinControl.SkinGroupBox skinGroupBox1;
        private System.Windows.Forms.ColumnHeader colID;
        private System.Windows.Forms.ColumnHeader colName;
        private System.Windows.Forms.ColumnHeader colStartTime;
        private System.Windows.Forms.ColumnHeader colArriveTime;
        private System.Windows.Forms.ColumnHeader colCost;
        private CCWin.SkinControl.SkinGroupBox skinGroupBox2;
        private CCWin.SkinControl.SkinToolStrip skinToolStrip1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel3;
        private System.Windows.Forms.ToolStripButton tbtnAddMember;
        private System.Windows.Forms.ToolStripButton tbtnSelAll;
        private System.Windows.Forms.ToolStripButton tbtnUnselAll;
        private System.Windows.Forms.ToolStripButton tbtnReverseSel;
        private System.Windows.Forms.ToolStripButton tbtnRemoveSel;
        private CCWin.SkinControl.SkinPanel skinPanel3;
        private CCWin.SkinControl.SkinPanel skinPanel2;
        private CCWin.SkinControl.SkinTextBox txtName;
        private CCWin.SkinControl.SkinLabel skinLabel2;
        private CCWin.SkinControl.SkinPanel skinPanel1;
        private CCWin.SkinControl.SkinTextBox txtID;
        private CCWin.SkinControl.SkinLabel skinLabel1;
        private CCWin.SkinControl.SkinPanel skinPanel4;
        private CCWin.SkinControl.SkinTextBox txtStartTime;
        private CCWin.SkinControl.SkinButton btnSetStartTime;
        private CCWin.SkinControl.SkinLabel skinLabel3;
        private CCWin.SkinControl.SkinPanel skinPanel5;
        private CCWin.SkinControl.SkinTextBox txtArriveTime;
        private CCWin.SkinControl.SkinButton btnSetArriveTime;
        private CCWin.SkinControl.SkinLabel skinLabel4;
        private PrintListView lstviewMembers;
        private System.Windows.Forms.ColumnHeader colLineID;
        private System.Windows.Forms.ColumnHeader colScore;
        private System.Windows.Forms.ColumnHeader colTag;
        private CCWin.SkinControl.SkinPanel skinPanel6;
        private CCWin.SkinControl.SkinTextBox txtLineID;
        private CCWin.SkinControl.SkinLabel skinLabel5;
        private CCWin.SkinControl.SkinPanel skinPanel7;
        private CCWin.SkinControl.SkinTextBox txtTag;
        private CCWin.SkinControl.SkinLabel skinLabel6;
        private System.Windows.Forms.ToolStripButton tbtnPrint;
        private System.Windows.Forms.ToolStripButton tbtnSave;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.Timer timerTimeSet;
        private CCWin.SkinControl.SkinSplitContainer skinSplitContainer2;
        private frmCompInfoSel info;
    }
}