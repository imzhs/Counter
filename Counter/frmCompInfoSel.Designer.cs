﻿namespace Counter
{
    partial class frmCompInfoSel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.comGroupType = new System.Windows.Forms.ComboBox();
            this.comProject = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.comComID = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.comGroupID = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(334, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "选择或输入当前比赛的详细信息";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "组别：";
            // 
            // comGroupType
            // 
            this.comGroupType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comGroupType.FormattingEnabled = true;
            this.comGroupType.Location = new System.Drawing.Point(59, 31);
            this.comGroupType.Name = "comGroupType";
            this.comGroupType.Size = new System.Drawing.Size(263, 20);
            this.comGroupType.TabIndex = 2;
            // 
            // comProject
            // 
            this.comProject.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comProject.FormattingEnabled = true;
            this.comProject.Location = new System.Drawing.Point(59, 57);
            this.comProject.Name = "comProject";
            this.comProject.Size = new System.Drawing.Size(263, 20);
            this.comProject.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 3;
            this.label3.Text = "项目：";
            // 
            // comComID
            // 
            this.comComID.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comComID.FormattingEnabled = true;
            this.comComID.Location = new System.Drawing.Point(59, 83);
            this.comComID.Name = "comComID";
            this.comComID.Size = new System.Drawing.Size(263, 20);
            this.comComID.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 86);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 12);
            this.label4.TabIndex = 5;
            this.label4.Text = "赛次：";
            // 
            // comGroupID
            // 
            this.comGroupID.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comGroupID.FormattingEnabled = true;
            this.comGroupID.Location = new System.Drawing.Point(59, 109);
            this.comGroupID.Name = "comGroupID";
            this.comGroupID.Size = new System.Drawing.Size(263, 20);
            this.comGroupID.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 112);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 7;
            this.label5.Text = "组次：";
            // 
            // frmCompInfoSel
            // 
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.comGroupID);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.comComID);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.comProject);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.comGroupType);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "frmCompInfoSel";
            this.Size = new System.Drawing.Size(334, 142);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        internal System.Windows.Forms.ComboBox comGroupType;
        internal System.Windows.Forms.ComboBox comProject;
        internal System.Windows.Forms.ComboBox comComID;
        internal System.Windows.Forms.ComboBox comGroupID;
    }
}